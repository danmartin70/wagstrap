<?php get_header(); ?>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-header-wrap">
<header class="page-title">
            <h1 class="single-title"><?php the_title();?></h1>
        </header>
</div>
 <div class="info-contain">
        <div class="page-content">
<div class="row-fluid">
  <div class="span9">
		<aside class="span2"><div class="authors author-<?php the_author_ID(); ?>"><span><?php the_author(); ?></span></div></aside>
    <article class="single-post span10">
   
       
      <?php the_date('l, F j, Y', '<small class="smdate clearfix"><em>', '</em></small>'); ?>
	  	<?php the_content(); ?>

	  	<hr>
		<?php comments_template(); ?>

	<?php endwhile; else: ?>
		<p><?php _e('Sorry, this page does not exist.'); ?></p>
	<?php endif; ?>

  </div>
  <div class="sidebar span3">
	<?php get_sidebar(); ?>  	
  </div>
</div>
  </div>
</div>

<?php get_footer(); ?>