<?php get_header(); ?>
          <div class="header-wrap">
          	<header>
<img class="img" src="/wp-content/themes/wagstrap/assets/img/banner.png">
</header>
        </div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>
	
<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>   

<?php get_footer(); ?>