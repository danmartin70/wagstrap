<?php get_header(); ?>

          <div class="page-header-wrap">
<header class="page-title">
            <h1>Writings</h1>
        </header>
                </div>
                <div class="info-contain">
        <div class="page-content">
<div class="row-fluid">
  <div class="span9">
    <?php
              // Blog post query
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    query_posts( array( 'post_type' => 'post', 'paged'=>$paged, 'showposts'=>0) );
    if (have_posts()) : while ( have_posts() ) : the_post(); ?>
     <div class="grouppost-entry row-fluid">
      
      <aside class="span2"><div class="authors author-<?php the_author_ID(); ?>"><span><?php the_author(); ?></span></div></aside>
    <article class="span10">
   
       
      <?php the_date('l, F j, Y', '<small class="smdate clearfix"><em>', '</em></small>'); ?>
  	<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></h1></a>
      
	<?php the_content(); ?>

  </article>
	 </div> 
<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>   
</div>
<div class="sidebar span3"><?php get_sidebar(); ?></div>
</div></div></div>
<?php get_footer(); ?>
