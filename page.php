<?php get_header(); ?>
          
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-header-wrap">
<header class="page-title">
            <h1><?php the_title();?></h1>
        </header>
</div>
<div class="info-contain">
        <div class="page-content">
<div class="row-fluid">
  <div class="span8">
	<?php the_content(); ?>
	</div></div></div>
	</div>
<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>   

<?php get_footer(); ?>