//custom js
$(document).ready(function(){
$(".header-wrap .img").css('opacity', .5).slideDown('slow', 'linear')
    .animate(
    { opacity: "1" },
    { queue: false, duration: 'fast' });


//start gear rotation

var lastScrollTop = 0;
var sdegree = 0;
$(window).scroll(function(event){
   var st = $(this).scrollTop();
   if (st > lastScrollTop){
         // downscroll code
     sdegree = sdegree + 5 ;
     var srotate = "rotate(" + sdegree + "deg)";
     $(".gear").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});
 
   } else {
      // upscroll code
     sdegree = sdegree - 5 ;
     var srotate = "rotate(" + sdegree + "deg)";
     $(".gear").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});
   }
   lastScrollTop = st;
});

//end gear rotation

});