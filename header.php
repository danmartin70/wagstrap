<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Wagmatic Studios</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    
    <link rel="shortcut icon" href="/wp-content/themes/wagstrap/assets/img/favicon.png">


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/wp-content/themes/wagstrap/assets/js/html5shiv.js"></script>
    <![endif]-->
<?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
    <!-- Fav and touch icons -->
    <script type="text/javascript" src="//use.typekit.net/qzh3oat.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
  </head>

  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="logo"><a href="/"><div class="gear"></div><div class="dog"></div></a></div>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <?php wp_nav_menu( array( 'container' => '', 'items_wrap' => '%3$s' ) ); ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    <div class="container-fluid">
  