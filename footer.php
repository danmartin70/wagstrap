    
<footer>
  <div class="info-contain">
    <img src="/wp-content/themes/wagstrap/assets/img/banner-bottom.png">
    <p class="tagline">Custom Handcrafted Digital Goods</p>
    <p class="tagline">Made in Maine</p>
  </div>
  <ul class="social">
              <li><a href="http://twitter.com/wagmatic" target="_blank">Twitter</a></li>
              <li class="social-middle"><a href="http://www.facebook.com/wagmaticstudios" target="_blank">Facebook</a></li>
              <li><a href="/contact">E-mail</a></li>
  </ul>
  <div class="legal">© 2013 Wagmatic Studios All Rights Reserved.</div>
</footer>
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="/wp-content/themes/wagstrap/assets/js/bootstrap.min.js"></script>
    <script src="/wp-content/themes/wagstrap/assets/js/jquery-1.8.2.min.js"></script>
    <script src="/wp-content/themes/wagstrap/assets/js/bootstrap-dropdown.js"></script>
    <script src="/wp-content/themes/wagstrap/assets/js/custom.js"></script>

<?php wp_footer(); ?>
  </body>
</html>